// 1) Цикл вказує на конструкцію, яка дозволяє виконувати блок коду декілька разів. Це дозволяє автоматизувати повторювані дії і використовувати один і той самий код для обробки різних даних або виконання однотипних завдань.
// 2)Цикл while: слово while
// Цикл for:  слово for
// Цикл do...while слова: do і while
// 3)Відмінність полягає в тому, що цикл do...while гарантує виконання блоку коду принаймні один раз, оскільки перевірка умови відбувається після виконання блоку коду. У while перевірка умови відбувається перед виконанням блоку коду, тому може статися так, що блок коду взагалі не виконається, якщо умова вже не виконується з самого початку.


// 1)
function isNumber(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
}
let number1, number2;
do {
    number1 = prompt("Введіть перше число:");
} while (!isNumber(number1));
do {
    number2 = prompt("Введіть друге число:");
} while (!isNumber(number2));
number1 = parseFloat(number1);
number2 = parseFloat(number2);
if (Number.isInteger(number1) && Number.isInteger(number2)) {
    let smallerNumber, largerNumber;

    if (number1 < number2) {
        smallerNumber = Math.floor(number1);
        largerNumber = Math.ceil(number2);
    } else {
        smallerNumber = Math.floor(number2);
        largerNumber = Math.ceil(number1);
    }

    console.log("Цілі числа від меншого до більшого:");
    for (let i = smallerNumber; i <= largerNumber; i++) {
        console.log(i);
    }
} else {
    console.log("Введені значення не є цілими числами.");
}

// 2)
function isEven(number) {
    return number % 2 === 0;
}
let userInput;
do {
    userInput = prompt("Введіть число:");
    userInput = parseInt(userInput);

    if (isNaN(userInput)) {
        console.log("Введіть правильне число.");
    } else {
        if (isEven(userInput)) {
            console.log("Введене число є парним.");
        } else {
            console.log("Введене число не є парним. Спробуйте ще раз.");
        }
    }
} while (!isEven(userInput));